#ifndef CHARACTERMANAGER_H
#define CHARACTERMANAGER_H
#include <string>
#include <vector>

namespace gtools
{
	class CharacterManager
	{
		std::string race = "";
		
		std::vector<std::pair<std::string,unsigned int>> inventory; //name of item, amount

		std::vector<std::string> flags;

		void GiveItem(std::string name, unsigned int amount);

		int CheckItem(std::string name, unsigned int amount);
		public:

		void SetRace(std::string race);

		int CheckFlag(std::string flag);

		void SetFlag(std::string flag);

		std::string ExtractConditional(std::string text);

		void FlagChoices(std::vector<std::string> * echoices, std::vector<unsigned long long> * eindex);
		
		std::string RemoveFlagChoice(std::string toclean);

		void GetFlags(std::string tocheck);

		void DebugPrintAll();
	};
}



#endif
