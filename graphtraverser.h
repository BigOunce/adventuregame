#ifndef GRAPHTRAVERSER_H //header guard
#define GRAPHTRAVERSER_H

#include <string>
#include <vector>
#include <iostream>
#include <fstream>


namespace gtools //graph traversing tools
{
	class GraphTraverser
	{
		unsigned long long linenumber = 0;
		std::vector<std::vector<std::string> * > * graphstore; //it has a fit if i use an array of vectors
		
		std::vector<unsigned long long> * nodechoiceindexes;

		std::vector<std::string> * choicetext;

		//possibly multi-thread ^^this bit

		static std::vector<std::string> * SplitLine(std::string * line);

		std::string * textprompt;

		void SetIndexes();

		void SetOptions();

		void SetTextPrompt();

		public:

		std::string * GetTextPrompt();

		std::vector<std::string> * GetChoiceText();

		std::vector<unsigned long long> * GetChoiceIndexes();

		void DebugPrintAll();
		
		unsigned long long getlinenumber();

		GraphTraverser(std::string path);

		~GraphTraverser();

		void GoToNode(unsigned long long jumpto); //jumps to node, for accessing next or loading a save
		int LoadGraph(std::string path);

		std::string GetPrompt();

		void DumpGraphstore();
	};
		

};

#endif //end header guard
