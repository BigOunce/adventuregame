#epic

adventuregame: main.o graphtraverser.o charactermanager.o
	g++ -o adventuregame main.o -lncurses graphtraverser.o charactermanager.o

charactermanager.o: charactermanager.h charactermanager.cpp
	g++ -c charactermanager.h charactermanager.cpp

main.o:
	g++ -c main.cpp -lncurses.h

graphtraverser.o: graphtraverser.h graphtraverser.cpp
	g++ -c graphtraverser.h graphtraverser.cpp

clean:
	rm *.o
	rm *.gch
	rm adventuregame
