#include "graphtraverser.h"
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <regex>

using namespace gtools;

std::string * gtools::GraphTraverser::GetTextPrompt()
{
	return this->textprompt;
}

std::vector<std::string> * gtools::GraphTraverser::GetChoiceText()
{
	return this->choicetext;
}

std::vector<unsigned long long> * gtools::GraphTraverser::GetChoiceIndexes()
{
	return this->nodechoiceindexes;
}

void gtools::GraphTraverser::DebugPrintAll()
{
	std::cout << "linenumber: " << this->linenumber << "\n";
	std::cout << "textprompt: " << *this->textprompt << "\n";
	for(unsigned int i = 0; i < choicetext->size(); i++)
	{
		std::cout << choicetext->at(i) << " choicetext at: " << i << " with index: " << nodechoiceindexes->at(i) << "\n";
	}
}


void gtools::GraphTraverser::SetTextPrompt()
{
	//std::cout << "fuck3\n";
	this->textprompt = &this->graphstore->at(this->linenumber)->at(this->graphstore->at(this->linenumber)->size() - 1);
}

void gtools::GraphTraverser::GoToNode(unsigned long long jumpto)
{
	this->linenumber = jumpto;
	this->SetIndexes();
	this->SetOptions();
	this->SetTextPrompt();
};

std::string gtools::GraphTraverser::GetPrompt()
{
	return *this->textprompt;
};

std::vector<std::string> * gtools::GraphTraverser::SplitLine(std::string * line) //probably causes a memory leak
{
	std::vector<std::string> * toreturn = new std::vector<std::string>();
	std::string temp = "";
	for (unsigned int i = 0; i < line->length(); i++)
	{
		if (line->at(i) == ',') //if it's the end of the line or the delimiter
		{
			toreturn->push_back(temp);
			temp = "";
			continue;
		}
		temp += line->at(i);
		if (i == line->length() - 1)
		{
			toreturn->push_back(temp);
			temp = "";
			continue;
		}
	}
	return toreturn;
};


int gtools::GraphTraverser::LoadGraph(std::string path)
{
	std::string line;
	std::ifstream * unfiltered = new std::ifstream();
	unfiltered->open(path);
	if(!unfiltered->is_open())   // if it fails to open
	{
		unfiltered->close();
		return 1;
	}
	while(std::getline(*unfiltered,line))
	{
		if (line[0] == '-') //if the line is a comment line
			continue;
		else
			this->graphstore->push_back(gtools::GraphTraverser::SplitLine(&line)); //splits the line into its constituent parts
	}
	unfiltered->close();
	delete(unfiltered);
	return 0;
};

gtools::GraphTraverser::GraphTraverser(std::string path)
{
	this->graphstore = new std::vector<std::vector<std::string> * >();
	try
	{
		this->LoadGraph(path);
		this->GoToNode(0);
	}
	catch(...)
	{
		throw;
	}
}

gtools::GraphTraverser::~GraphTraverser()
{
	unsigned long long size = this->graphstore->size();
	for (unsigned long long i = 0; i < size; i++)
	{
		delete(this->graphstore->at(i));
	}
	delete(this->graphstore);
	delete(this->choicetext);
	delete(this->nodechoiceindexes);
}

unsigned long long gtools::GraphTraverser::getlinenumber()
{
	return this->linenumber;
}
void gtools::GraphTraverser::SetIndexes()
{
	delete(this->nodechoiceindexes);
	std::vector<std::string> * subgraph = this->graphstore->at(this->linenumber);
	std::vector<unsigned long long> * topass = new std::vector<unsigned long long>();
	unsigned int optionsize = subgraph->size() / 2; //this is where the option indexes end and the text begins
	for(unsigned int i = 1; i < optionsize; i++)
	{
		topass->push_back(std::stoi(subgraph->at(i)));
	}
	this->nodechoiceindexes = topass;
}

void gtools::GraphTraverser::SetOptions()
{
	delete(this->choicetext);
	std::vector<std::string> * topass = new std::vector<std::string>();
	std::vector<std::string> * subgraph = this->graphstore->at(this->linenumber);
	unsigned int optionsize = subgraph->size() / 2;
	for(unsigned int i = optionsize; i < subgraph->size() - 1; i++) //getting the second half of the line but not the last element
		topass->push_back(subgraph->at(i));
	this->choicetext = topass;
}
