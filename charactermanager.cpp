#include "charactermanager.h"
#include <string>
#include <vector>
#include <iostream> //debug

using namespace gtools;

std::string gtools::CharacterManager::ExtractConditional(std::string text) //this removes items from the text prompt if the appropriate flag is not found.
{
	unsigned int index = 0;
	std::string temp = "";
	std::string temp2 = "";
	int found = 0;
	while(!found && index < text.length())
	{
		if(text[index] == '$') // $ is the flag for text that should only appear if a condition is met
		{
			//std::cout << "conditional found\n";
			index++;
			int found2 = 0;
			while(!found2 && index < text.length()) //looks for conditional text after the flag
			{
				//std::cout << "text found\n";
				if (text[index] == '(') 
				{
					if(this->CheckFlag(temp2)) //if the condition is satisfied
					{
						//std::cout << "condition satisfied\n";
						index++;
						std::string temp3 = "";
						int found3 = 0;
						while(!found3 && index < text.length()) //append the required text to the prompt
						{
							if (text[index] == ')')
							{
								found3 = 1;
								return temp += temp3;
							}
							temp3 += text[index];
							index++;
						}
					}
				}
				temp2 += text[index];
				index++;
			}
		}
		temp += text[index];
		index++;
	}
	return temp;
}

std::string gtools::CharacterManager::RemoveFlagChoice(std::string toclean)
{
	std::string toreturn = "";
	int index = 0;
	if(toclean[0] == '$') //if a required flag for the choice is detected
	{
		while(toclean[index] != ' ') //skip past it
			index++;
		index++;
	}
	while(index < toclean.length()) //get all text until a flag at the end
	{
		if(toclean[index] == '#' || toclean[index] == '@' || toclean[index] == '%')
		{
			break;
		}
		toreturn += toclean[index];
		index++;
	}
	return toreturn; //send to ncurses for display
}

void gtools::CharacterManager::FlagChoices(std::vector<std::string> * echoices, std::vector<unsigned long long> * eindex)
{
	std::vector<unsigned int> indexes;
	int size = echoices->size();
	int found = 0;
	for(unsigned int i = 0; i < size; i++)
	{
		std::string temp = "";
		std::string flagtemp = "";
		int index = 1;
		int negation = 0;
		while(index < echoices->at(i).length()) //gets the flag
		{
			if(echoices->at(i)[0] != '$')
				break;
			if(echoices->at(i)[1] == '!' && !negation)
			{
				//std::cout << "detected negation\n";
				index++;
				negation = 1;
			}
			if(echoices->at(i)[index] == ' ')
				break;
			flagtemp += echoices->at(i)[index];
			index++;
		}
		if((this->CheckFlag(flagtemp) && !negation) || flagtemp == "" || (!this->CheckFlag(flagtemp) && negation)) //catches the circumstances where no deletion is needed
			continue;
		else //if for deletion (probably don't need the else clause but too afraid to touch it)
		{
			found = 1;
			indexes.push_back(i);
		}
	}
	if(found)
	{
		int size2 = indexes.size(); //using the vector size here will cause the goal value to decrease
		for(int i = 0; i < size2; i++) //deletes unneeded items from the temp array (pointers are great)
		{
			echoices->erase(echoices->begin() + (indexes.at(0) - i));
			eindex->erase(eindex->begin() + (indexes.at(0) - i));
			indexes.erase(indexes.begin());
		}
	}
}

void gtools::CharacterManager::GetFlags(std::string tocheck) //Gets the flags required 
{
	int index = 0;
	char flagstore;
	while(index < tocheck.length() && !(tocheck[index] == '%' || tocheck[index] == '@' || tocheck[index] == '#')) // skips irrelevant text
	{
		index++;
	}
	flagstore = tocheck[index];
	switch(flagstore) //decides what to do
	{
		case('%'):
			{
				index++;
				std::string temp = "";
				while(index < tocheck.length() && !(tocheck[index] == ' '))
				{
					temp += tocheck[index];
					index++;
				}
				this->SetFlag(temp);
				if(temp == "MINITH")
					this->SetFlag("MOUNTAIN_PATH");
				break;
			}
		case('@'):
			{
				index++;
				std::string temp2 = "";
				std::string strcount = "";
				while(index < tocheck.length() && !(tocheck[index] == ' ' || tocheck[index] == '('))
				{
					temp2 += tocheck[index];
					index++;
				}
				if(index < tocheck.length() && tocheck[index] == '(')
				{
					index++;
					while(index < tocheck.length() && tocheck[index] != ')')
					{
						strcount += tocheck[index];
						index++;
					}
					this->GiveItem(temp2, std::stoi(strcount));
				}
				break;
			}
		case('#'):
			{
				index++;
				std::string temp3 = "";
				std::string strcount2 = "";
				while(index < tocheck.length() && !(tocheck[index] == ' ' || tocheck[index] == '('))
				{
					temp3 += tocheck[index];
					index++;
				}
				if(index < tocheck.length() && tocheck[index] == '(')
				{
					index++;
					while(index < tocheck.length() && tocheck[index] != ')')
					{
						strcount2 += tocheck[index];
						index++;
					}
					this->CheckItem(temp3, std::stoi(strcount2));
				}
				break;
			}
	}
	index ++;
	if(index < tocheck.length())
	{
		while(index < tocheck.length() && !(tocheck[index] == '%' || tocheck[index] == '@' || tocheck[index] == '#')) // skips irrelevant text
		{
			index++;
		}
		flagstore = tocheck[index];
		switch(flagstore) //decides what to do
		{
			case('%'):
				{
					index++;
					std::string temp = "";
					while(index < tocheck.length() && !(tocheck[index] == ' '))
					{
						temp += tocheck[index];
						index++;
					}
					this->SetFlag(temp);
					break;
				}
			case('@'):
				{
					index++;
					std::string temp2 = "";
					std::string strcount = "";
					while(index < tocheck.length() && !(tocheck[index] == ' ' || tocheck[index] == '('))
					{
						temp2 += tocheck[index];
						index++;
					}
					if(index < tocheck.length() && tocheck[index] == '(')
					{
						index++;
						while(index < tocheck.length() && tocheck[index] != ')')
						{
							strcount += tocheck[index];
							index++;
						}
						this->GiveItem(temp2, std::stoi(strcount));
					}
					break;
				}
			case('#'):
				{
					index++;
					std::string temp3 = "";
					std::string strcount2 = "";
					while(index < tocheck.length() && !(tocheck[index] == ' ' || tocheck[index] == '('))
					{
						temp3 += tocheck[index];
						index++;
					}
					if(index < tocheck.length() && tocheck[index] == '(')
					{
						index++;
						while(index < tocheck.length() && tocheck[index] != ')')
						{
							strcount2 += tocheck[index];
							index++;
						}
						this->CheckItem(temp3, std::stoi(strcount2));
					}
					break;
				}
		}

	}
}

void gtools::CharacterManager::DebugPrintAll()
{
	for(int i = 0; i < this->inventory.size(); i++)
		std::cout << "item: " <<  this->inventory.at(i).first << " amount: " << this->inventory.at(i).second << "\n";
	for(int i = 0; i < this->flags.size(); i++)
		std::cout << "flag: " << this->flags.at(i);
}
void gtools::CharacterManager::SetRace(std::string race) //deprecated but kept for posterity
{
	this->race = race;
	this->SetFlag(race);
	if(race == "MINITH")
		this->SetFlag("MOUNTAINPATH");
}

int gtools::CharacterManager::CheckItem(std::string name, unsigned int amount)
{
	//Technically unsafe but the CSV should be structured such that false entries are not possible;
	//returns 0 if it's successful
	int index = 0;
	int found = 0;
	while(!found && index < this->inventory.size())
	{
		if(this->inventory.at(index).first == name)
		{
			found = 1;
			break;
		}
		index++;
	}
	if(!found) //no item found at all
		return 1;
	if(found && this->inventory.at(index).second <=0) //item found but quantity is 0
		return 1;
	if(found && amount == 0) //item found and no takeaway
		return 0;
	if(found && amount > 0)
	{
		if(this->inventory.at(index).second - amount < 0) //if found but insufficent amount 
			return 2;
		this->inventory.at(index).second = inventory.at(index).second - amount; //success
		return 0;
	}
	return 3;
}

void gtools::CharacterManager::GiveItem(std::string name, unsigned int amount)
{
	int index = 0;
	int found = 0;
	while(!found && index < this->inventory.size())
	{
		if(this->inventory.at(index).first == name)
			found = 1;
		index++;
	}
	if(found)
	{
		if(amount > 0)
			this->inventory.at(index).second += amount;
		else
		{
			this->inventory.at(index).second++;
		}
	}
	if (amount > 0)
	{
		std::pair<std::string,unsigned int> toadd;
		toadd.first = name;
		toadd.second = amount;
		this->inventory.push_back(toadd);
		return;
	}
	std::pair<std::string,unsigned int> toadd;
	toadd.first = name;
	toadd.second = 1;
	this->inventory.push_back(toadd);
}

int gtools::CharacterManager::CheckFlag(std::string flag) //TODO:remake with regexes
{
	int index = 0;
	int found = 0;
	while (!found && index < this->flags.size())
	{
		if(this->flags.at(index) == flag)
			found = 1;
		index++;
	}
	return found;
}

void gtools::CharacterManager::SetFlag(std::string flag)
{
	if(CheckFlag(flag))
		return;
	this->flags.push_back(flag);
}
