#include <ncurses.h>
#include <iostream>
#include <string>
#include "graphtraverser.h"
#include "charactermanager.h"
	
std::vector<std::string> deepcopy(std::vector<std::string> * tocopy) //copies the vector because it causes some weird behaviour otherwise
{
	std::vector<std::string> toreturn;
	for(unsigned int i = 0; i < tocopy->size(); i++)
		toreturn.push_back(tocopy->at(i));
	return toreturn;
}

std::vector<unsigned long long> deepcopyindex (std::vector<unsigned long long> * tocopy)
  {
          std::vector<unsigned long long> toreturn;
          for(unsigned int i = 0; i < tocopy->size(); i++)
                  toreturn.push_back(tocopy->at(i));
          return toreturn;
  }

void clearmenubox(WINDOW * menubox)
{
	for (int i = 1; i < 12; i++)
	{
		wmove(menubox, i, 1);
		wclrtobot(menubox);
	}
}

int drawmenubox(WINDOW * menubox, std::vector<std::string> * choices, std::vector<unsigned long long> * choiceindex, gtools::CharacterManager * manager, gtools::GraphTraverser * traverser)
{
	clearmenubox(menubox);
	std::vector<std::string> echoices = deepcopy(choices);
	std::vector<unsigned long long> eindex = deepcopyindex(choiceindex);
	manager->FlagChoices(&echoices, &eindex);
	init_pair(1,COLOR_MAGENTA,COLOR_BLACK);
	wattron(menubox, COLOR_PAIR(1));
	box(menubox, 0, 0);
	wattroff(menubox, COLOR_PAIR(1));
	wrefresh(menubox);

	int choice;
	int highlight = 0;

	while(1)
	{
		//draws text inside the input window
		for(int i = 0; i < echoices.size();i++)
		{
			if(i == highlight)
				wattron(menubox, A_REVERSE);
			mvwprintw(menubox, i + 1, 1, manager->RemoveFlagChoice(echoices.at(i)).c_str());
			wattroff(menubox, A_REVERSE);
		}
		wrefresh(menubox);

		choice = wgetch(menubox);

		switch(choice)
		{
			case KEY_UP:
				highlight--;
				if(highlight < 0)
					highlight = 0;
				break;
			case KEY_DOWN:
				highlight++;
				if(highlight > echoices.size() -1)
					highlight = echoices.size() -1;
				break;
			default:
				break;
		}

		if(choice == 10)
			break;

	}

	manager->GetFlags(echoices.at(highlight));

	return eindex[highlight];
}

void mainmenu(int ymax, int xmax) // all hail find and replace
{
	clear();
	std::vector<std::string> list;
	std::string text = "Press any key to continue or press Ctrl+C to quit at any point";
	list.push_back(" ___  ___  ________   ________   ________  _____ ______   _______   ________          ________  ___  ___  _______   ________  _________   ");
	list.push_back("|\\  \\|\\  \\|\\   ___  \\|\\   ___  \\|\\   __  \\|\\   _ \\  _   \\|\\  ___ \\ |\\   ___ \\        |\\   __  \\|\\  \\|\\  \\|\\  ___ \\ |\\   ____\\|\\___   ___\\ ");
	list.push_back("\\ \\  \\\\\\  \\ \\  \\\\ \\  \\ \\  \\\\ \\  \\ \\  \\|\\  \\ \\  \\\\\\__\\ \\  \\ \\   __/|\\ \\  \\_|\\ \\       \\ \\  \\|\\  \\ \\  \\\\\\  \\ \\   __/|\\ \\  \\___|\\|___ \\  \\_| ");
	list.push_back(" \\ \\  \\\\\\  \\ \\  \\\\ \\  \\ \\  \\\\ \\  \\ \\   __  \\ \\  \\\\|__| \\  \\ \\  \\_|/_\\ \\  \\ \\\\ \\       \\ \\  \\\\\\  \\ \\  \\\\\\  \\ \\  \\_|/_\\ \\_____  \\   \\ \\  \\  ");
	list.push_back("  \\ \\  \\\\\\  \\ \\  \\\\ \\  \\ \\  \\\\ \\  \\ \\  \\ \\  \\ \\  \\    \\ \\  \\ \\  \\_|\\ \\ \\  \\_\\\\ \\       \\ \\  \\\\\\  \\ \\  \\\\\\  \\ \\  \\_|\\ \\|____|\\  \\   \\ \\  \\ ");
	list.push_back("   \\ \\_______\\ \\__\\\\ \\__\\ \\__\\\\ \\__\\ \\__\\ \\__\\ \\__\\    \\ \\__\\ \\_______\\ \\_______\\       \\ \\_____  \\ \\_______\\ \\_______\\____\\_\\  \\   \\ \\__\\");
	list.push_back("    \\|_______|\\|__| \\|__|\\|__| \\|__|\\|__|\\|__|\\|__|     \\|__|\\|_______|\\|_______|        \\|___| \\__\\|_______|\\|_______|\\_________\\   \\|__|");
	list.push_back("                                                                                               \\|__|                  \\|_________|        ");
	int startx = (xmax / 2) - (list.at(0).length() / 2);
	int starty = (ymax / 2) - (list.size() / 2);
	for(unsigned int i = 0; i < list.size(); i++)
		mvprintw(starty + i, startx, list.at(i).c_str());
	mvprintw(starty + list.size(), (xmax / 2) - (text.length() / 2), text.c_str());	
	getch();
	clear();
}


void displayprompt(std::string textprompt, int ymax, int xmax, gtools::CharacterManager * manager) //Displays the text prompt with special line break format
{
	textprompt = manager->ExtractConditional(textprompt);
	int starty = ymax / 2;
	std::vector<std::string> list;
	std::string temp = "";
	for (unsigned int i = 0; i < textprompt.length(); i++) //Searches for line break character 
	{
		if (textprompt[i] == '>')
		{
			list.push_back(temp);
			temp = "";
			continue;
		}
		temp += textprompt[i];
		if (i == textprompt.length() - 1)
		{
			list.push_back(temp);
			continue;
		}
	}
	for (unsigned int i = 0; i < list.size(); i++)
	{
		move(starty + i, (xmax / 2) - (list[i].length() / 2));
		clrtoeol();
		printw(list[i].c_str());
	}
}

void playgame(gtools::GraphTraverser * traverser, gtools::CharacterManager * manager, WINDOW * menubox, int xmax, int ymax)
{
	while(1)
	{
		clear();
		displayprompt(*traverser->GetTextPrompt(), ymax, xmax, manager);
		refresh();
		wrefresh(menubox);
		unsigned long long next = drawmenubox(menubox, traverser->GetChoiceText(), traverser->GetChoiceIndexes(), manager, traverser);
		wrefresh(menubox);
		if(next == 65535)
			break;
		traverser->GoToNode(next);
		refresh();
	}
}

int main(int argc, char ** argv)
{
	gtools::GraphTraverser * traverser = new gtools::GraphTraverser("./real.csv");
	gtools::CharacterManager * manager;


	/*for (unsigned int i = 0; i < 62; i++)
	{
		std::cout << "Checking Line: " << i << "\n";
		traverser->GoToNode(i);
		traverser->DebugPrintAll();
	}*/ //LINE CHECKER DEBUG
	
	//traverser->GoToNode(15);
	//std::cout << *traverser->GetTextPrompt() << "\n";

	initscr(); //starts ncurses

	noecho();
	cbreak();
	curs_set(0);
	if(!has_colors())
	{
		printw("Terminal does not support colours");
		getch();
		return 1;
	}

	start_color();

	//get terminal information
	
	int xmax,ymax;

	getmaxyx(stdscr, ymax, xmax);

	while(1)
	{
		manager = new gtools::CharacterManager();
		traverser->GoToNode(0);
		mainmenu(ymax, xmax);
		// make little input box

		WINDOW * menubox = newwin(12, xmax - (xmax * 0.2), ymax - (ymax / 5), xmax - (xmax * 0.9));
		keypad(menubox, true);

		playgame(traverser, manager, menubox, xmax, ymax);
		delete(manager);
	}
	
	getch();

	endwin(); //ends ncurses
	
	std::cout << "\n\nThanks for playing\n";

	return 0;
}
